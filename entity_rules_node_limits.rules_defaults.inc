<?php
/**
 * @file
 * entity_rules_node_limits.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function entity_rules_node_limits_default_rules_configuration() {
  $items = array();
  $items['rules_delete_oldest_node_if_over_limit'] = entity_import('rules_config', '{ "rules_delete_oldest_node_if_over_limit" : {
      "LABEL" : "Delete oldest node if over limit",
      "PLUGIN" : "rule",
      "TAGS" : [ "entity_rules_create" ],
      "REQUIRES" : [ "rules", "views_bulk_operations" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "node" },
        "continue" : { "label" : "Continue Rules", "type" : "boolean" },
        "limit" : { "label" : "Limit", "type" : "integer" },
        "show_message" : { "label" : "Show Delete Message", "type" : "boolean" }
      },
      "IF" : [
        { "views_bulk_operations_condition_result_count" : {
            "view" : "node_count|default",
            "args" : "[entity:type]\\r\\n[entity:author:uid]",
            "minimum" : [ "limit" ]
          }
        }
      ],
      "DO" : [
        { "views_bulk_operations_action_load_list" : {
            "USING" : {
              "view" : "oldest_node|default",
              "args" : "[entity:content-type]\\r\\n[entity:author:uid]\\r\\n"
            },
            "PROVIDE" : { "entity_list" : { "entity_list" : "A list of entities" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entity-list" ] },
            "ITEM" : { "list_item" : "Current list item" },
            "DO" : [
              { "entity_delete" : { "data" : [ "list-item" ] } },
              { "component_rules_send_message_if_true" : {
                  "send" : [ "show-message" ],
                  "text" : "You have created over [limit:value] [list-item:content-type]s. Your oldest [list-item:content-type], [list-item:title]."
                }
              }
            ]
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "continue" ]
    }
  }');
  $items['rules_limit_all_nodes'] = entity_import('rules_config', '{ "rules_limit_all_nodes" : {
      "LABEL" : "Limit all nodes",
      "PLUGIN" : "and",
      "TAGS" : [ "entity_rules_form_access" ],
      "REQUIRES" : [ "views_bulk_operations" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "node" },
        "node_limit" : { "label" : "Node Limit", "type" : "integer" }
      },
      "NOT AND" : [
        { "views_bulk_operations_condition_result_count" : {
            "view" : "node_count|default",
            "args" : "all\\r\\nall",
            "minimum" : [ "node-limit" ]
          }
        }
      ]
    }
  }');
  $items['rules_limit_all_per_user'] = entity_import('rules_config', '{ "rules_limit_all_per_user" : {
      "LABEL" : "Limit all per user",
      "PLUGIN" : "and",
      "TAGS" : [ "entity_rules_form_access" ],
      "REQUIRES" : [ "rules", "views_bulk_operations" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "node" },
        "node_limit" : { "label" : "Node Limit", "type" : "integer" }
      },
      "NOT AND" : [
        { "views_bulk_operations_condition_result_count" : {
            "view" : "node_count|default",
            "args" : "all\\r\\n[entity:author:uid]\\r\\n",
            "minimum" : [ "node-limit" ]
          }
        }
      ]
    }
  }');
  $items['rules_limit_total_per_type'] = entity_import('rules_config', '{ "rules_limit_total_per_type" : {
      "LABEL" : "Limit total per type",
      "PLUGIN" : "and",
      "TAGS" : [ "entity_rules_form_access" ],
      "REQUIRES" : [ "rules", "views_bulk_operations" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "node" },
        "node_limit" : { "label" : "Node Limit", "type" : "integer" }
      },
      "NOT AND" : [
        { "views_bulk_operations_condition_result_count" : {
            "view" : "node_count|default",
            "args" : "[entity:type]\\r\\nall\\r\\n",
            "minimum" : [ "node-limit" ]
          }
        }
      ]
    }
  }');
  $items['rules_limit_user_per_type'] = entity_import('rules_config', '{ "rules_limit_user_per_type" : {
      "LABEL" : "Limit user per type",
      "PLUGIN" : "and",
      "TAGS" : [ "entity_rules_form_access" ],
      "REQUIRES" : [ "rules", "views_bulk_operations" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "node" },
        "node_limit" : { "label" : "Node Limit", "type" : "integer" }
      },
      "NOT AND" : [
        { "views_bulk_operations_condition_result_count" : {
            "view" : "node_count|default",
            "args" : "[entity:type]\\r\\n[node:author:uid]\\r\\n",
            "minimum" : [ "node-limit" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_message_if_true'] = entity_import('rules_config', '{ "rules_send_message_if_true" : {
      "LABEL" : "Send Message if true",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "send" : { "label" : "Send", "type" : "boolean" },
        "text" : { "label" : "Text", "type" : "text" }
      },
      "IF" : [ { "data_is" : { "data" : [ "send" ], "value" : 1 } } ],
      "DO" : [ { "drupal_message" : { "message" : [ "text" ] } } ]
    }
  }');
  return $items;
}
